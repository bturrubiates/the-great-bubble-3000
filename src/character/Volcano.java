package character;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import utils.DataLoader;

/**
 * Functionality for the Volcano sprite used on the loading screen.
 * @author Benjamin Turrubiates, Ky Brabson
 *
 */
public class Volcano extends Character{
    
	/**
	 * Construct a volcano character with the default scale and position.
	 */
    public Volcano() {
    	this.init();
    }
    
    /**
     * Construct a volcano character with the given values.
     * @param x X position of the volcano.
     * @param y Y position of the volcano.
     * @param scale Scale that should be used on the original image.
     */
	public Volcano(int x,int y,float scale) {
		position = new Vector2f(x, y);
		characterScale = scale;
		this.init();
	}
	
	/**
	 * Takes care of initializing the volcano character.
	 */
	public void init() {
		duration = 100;
		characterStep = 4;
		spritesPerColumn = 1;
		character = DataLoader.VOLCANO_IMAGE;
		super.init();
		
		
		//move right animation
		Image[] moveRight= {sheet.getSprite(2,0) ,sheet.getSprite(1,0) , 
				sheet.getSprite(0,0) , sheet.getSprite(1,0) ,
				sheet.getSprite(2,0) , sheet.getSprite(3,0) ,
				sheet.getSprite(4,0) , sheet.getSprite(3,0)};
		super.characterMoveRight = new Animation(moveRight, duration, true);
		super.animation = super.characterMoveRight;
		
		
	}
	
	/**
	 * Render the volcano at the given position.
	 */
	public void render() {
		animation.draw(position.x, position.y);		
	}
}
