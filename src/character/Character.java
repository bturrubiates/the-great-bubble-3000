package character;

import java.awt.Rectangle;
import java.util.ArrayList;
import level.Block;
import level.Level;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * All of the entities in the game should inherit from this class. 
 * @author Benjamin Turrubiates, Ky Brabson, and Timmy Miles
 *
 */
public abstract class Character {
	protected Vector2f position = new Vector2f(80, 200);
	protected Vector2f velocity = new Vector2f(0, 0);
    float characterStep = 0.65f; 
    float characterScale = 0.3f;
    private boolean dead = false;
    
    protected Image character, characterScaled;
    
    private enum direction {
    	RIGHT(-1), LEFT(1);
    	
    	private int numVal;
    	
    	direction(int numVal) {
    		this.numVal = numVal;
    	}
    	
    	public int getNumVal() {
    		return numVal;
    	}
    };
    
    private direction currentDirection = direction.RIGHT;
    
    protected SpriteSheet sheet;
    protected float spriteSheetWidth, spriteSheetHeight;
    protected int spritesPerRow = 5;
    protected int spritesPerColumn;
    protected int spriteWidth, spriteHeight;
    
    protected int jump = 0;
    protected float jumpSpeed = 0.0f;
    protected boolean jumping = false;
    protected boolean left = false;
    
    protected Animation animation;
	protected Animation characterMoveLeft, characterMoveRight;
	protected Animation characterJumpLeft, characterJumpRight;
	protected Animation characterDie, enemyDie;
	protected Animation characterEatLeft, characterEatRight;
	protected Animation characterFireLeft, characterFireRight;
	
    protected int duration, jumpDuration;
    
    /**
     * Responsible for setting up the sprites and character images.
     */
    protected void init() {
		//anti-aliasing
	    character.setFilter(Image.FILTER_NEAREST);
	    characterScaled = character.getScaledCopy(characterScale);
		
		spriteSheetWidth = characterScaled.getWidth();
		spriteSheetHeight = characterScaled.getHeight();
	    
		spriteWidth = (int)(spriteSheetWidth / spritesPerRow);
		spriteHeight = (int)(spriteSheetHeight / spritesPerColumn);
		
		//image, size of block
		sheet = new SpriteSheet(characterScaled, spriteWidth, spriteHeight);
	
    }

    /**
     * Returns the hitbox for the current entity. 
     * @return The hitbox for the entity represented as a rectangle.
     */
    public Rectangle getHitBox() {
        return new Rectangle(
        		(int) Math.round(position.x),
        		(int) Math.round(position.y),
        		spriteWidth, spriteHeight);
      }
    
    /**
     * 
     * @return Whether the player should be dead or not.
     */
    public boolean isDead() {
    	return dead;
    }
    
    /**
     * 
     * @return Returns a vector of the centered X and Y
     */
    public Vector2f getCenter() {
    	return new Vector2f((position.x + (spriteWidth / 2)),
    			(position.y + (spriteHeight / 2)));
    }
    
    /**
     * 
     * @return Returns the top left X value of the player sprite
     */
    public int getX() {
    	return (int) Math.round(position.x);
    }
    
    /**
     * 
     * @return Returns the top left Y value of the player sprite
     */
    public int getY() {
    	return (int) Math.round(position.y);
    }
    
    /**
     * 
     * @return Returns the width of the sprite
     */
    public int getWidth() {
    	return spriteWidth;
    }
    
    /**
     * 
     * @return Returns the height of the sprite
     */
    public int getHeight() {
    	return spriteHeight;
    }
    
    /**
     * 
     * @param position A vector representing the new position of the player
     */
    public void setPosition(Vector2f position) {
    	this.position = position;
    }
    
    /**
     * 
     * @param x An integer representing the new top left X value
     */
    public void setX(int x) {
    	position.x = (float) x;
    }
    
    /**
     * 
     * @param collidingBlock Check if the box is a spike and if so mark the
     * player dead.
     */
    public void checkDeath(Block collidingBlock) {
    	if(collidingBlock.getBlockType() == "SPIKE")
    		dead = true;
    }
    
    /**
     * Corrects for the fact that after every update the position of the player
     * could be inside the hitbox of another object. Not completely correctly
     * implemented. The blocks in the level are iterated through, and a 
     * vector of compensations are created depending on whether the player is
     * facing left, or right as well as which direction the velocity is and
     * also offsetting by the smallest difference in either the x or the y 
     * position. 
     * @param level Level that the character is currently playing.
     * @return A vector representing the corrections that need to be applied
     * to the position of the character.
     */
    public Vector2f compensateCollision(Level level) {
    	ArrayList<Rectangle> collidingRectangles = new ArrayList<Rectangle>();
    	Vector2f compensations = new Vector2f(0, 0);
    	for(Block block : level.getBlockList()) {
    		// Check if the character is colliding with the current block
    		if(getHitBox().intersects(block.getRectangularHitBox())) {
    			// Mark the character dead if the block is a spike
    			checkDeath(block);
    			// Add the block to the list that will be used to build 
    			// compensations
    			collidingRectangles.add(
    				getHitBox().intersection(block.getRectangularHitBox()));
    		}
    	}
    	// For each of the intersection boxes, figure out which dimension
    	// the width or the height is smaller and use that to compensate
    	// the character position. So if the width is smaller than the height
    	// then we will move the character by the width so it is no longer
    	// intersecting. This method has its bugs.
    	for(Rectangle rectangle : collidingRectangles) {
    		if(rectangle.width < rectangle.height) {
    			compensations.x += (rectangle.width * 
    					currentDirection.getNumVal());
    		} else {
    			// If we are moving down, then compensate upward by the height
    			// of the intersection box
    			if(velocity.y > 0)
    				compensations.y -= rectangle.height;
    			// If we are moving up, then compensate downward by the height
    			// of the intersection box
    			else if(velocity.y < 0)
    				compensations.y += rectangle.height;
    		}
    	}
    	return compensations;
    }
    
    /**
     * Attempts to calculate whether the player is on top of a platform.
     * @param level level the player is currently playing.
     * @return Whether the character is on a platform or not. 
     */
    public boolean collidingBelow(Level level) {
    	if(velocity.y < 0)
    		return false;
    	Rectangle intersection;
    	for(Block block : level.getBlockList()) {
    		// Request a rectangle representing the intersection of the two
    		// objects
    		intersection = 
    				getHitBox().intersection(block.getRectangularHitBox());
    		// If the box is empty there is no intersection
    		if(!intersection.isEmpty()) {
    			// If the width is larger then that means most likely we are
    			// either intersecting a box from the top or the bottom. 
    			if(intersection.width > intersection.height) {
    				// Attempt to check if we are lower than the block
    				if(getY() > block.getPosition().y)
    					return false;
    				else
    					return true;
    			}
    		}
    	}
    	return false;
    }
    
    /**
     * Logic for colliding with enemy
     * @return an integer value representing value of hit
     */
    public int killEnemy(FireGuy enemy){
    	enemy.character.getWidth();
    	Vector2f collision;
    	// Check if we are coming from the right
    	if(position.x > enemy.getX()){ 
    		collision = new Vector2f(((position.x + 30) - (enemy.getX() + 30)), 
    				(enemy.getY() + 30) - (position.y + 30)); // Uses center    		
    		if(collision.length() < 60){ // 170 = (240\sqrt{2}))
    			if(Math.atan( collision.y/ collision.x) > 0.45){
    				return 1; // Kill enemy
    			} else{
    				return 2;
    			}
    		} else{
    			return 0; // DO nothing
    		}
    		
    	} else { // On top or coming from left
    		collision = new Vector2f( (enemy.getX() + 30) - 
    				(position.getX() + 30), ((enemy.getY() + 30) - 
    						(position.y + 30)));
    		if(collision.length() < 60){ // 678 = (240\sqrt{2}) + (240\sqrt{2})		
    			if((Math.atan( collision.y / collision.x)) > 0.45){
    				return 1; // Kill enemy;
    			}
    			else{
    				return 2;
    			}
    		}
    		else{ // Not in range
    			return 0; // DO nothing
    		}
    	}
    }
    
    /**
     * Takes care of rendering the character taking the X position that the
     * viewing rectangle (camera) is currently located at. 
     * @param viewingX X position of the viewing rectangle.
     */
	public void render(int viewingX) {
		animation.draw(position.x - viewingX, position.y);
	}
    
    /**
     * Logic for moving the character.
     * @param gc 
     * @param sb 
     * @param delta Time between frames used to provide a framerate independent
     * movement.
     */
    public void move(GameContainer gc, StateBasedGame sb, int delta,
    		Level level) {
		Input input = gc.getInput();
		if(input.isKeyDown(Input.KEY_LEFT)) {
			velocity.x = - characterStep * delta;
			currentDirection = direction.LEFT;
			animation = characterMoveLeft;
		} else if (input.isKeyDown(Input.KEY_RIGHT)) {
			velocity.x = + characterStep * delta;
			currentDirection = direction.RIGHT;
			animation = characterMoveRight;
		}
		if(input.isKeyDown(Input.KEY_UP) && !jumping) {
			jumping = true;
			velocity.y = -15.0f;
		}
		if(jumping) {
			switch(currentDirection) {
				case RIGHT:
					animation = characterJumpRight;
					break;
				case LEFT:
					animation = characterJumpLeft;
					break;
			}
		}
		position.add(velocity);
		/*
		 * Need to actually detect if i'm colliding on something and not just
		 * with something on the sides. 
		 */
		if(collidingBelow(level)) {
			jumping = false;
			velocity.y = 0;
			switch(currentDirection) {
				case RIGHT:
					animation = characterMoveRight;
					break;
				case LEFT:
					animation = characterMoveLeft;
					break;
			}
		} else {
			velocity.y += .09f * delta;
		}
		position.add(compensateCollision(level));
		velocity.x = 0;
		
		/* Killing Enemy */
		
		for(int i = 0; i < level.getEnemyList().size(); i++){
			int killStatus = killEnemy(level.getEnemyList().get(i));
			if(killStatus == 1){ // Kill Enemy
				level.getEnemyList().remove(i);
				int currentScore = Bubble.score;
				Bubble.score = currentScore + 100;
				
			}
			else if(killStatus == 2){// Kill Bubble
				dead = true;
			}
			else{
				// Do nothing
			}
		}
		
		if(position.y > 600)
			dead = true;
		if(position.x > 9500)
			level.setWon(true);
		
		if(dead){
			animation = characterDie;
		}
		
    }
}
