package character;

import level.Level;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import utils.DataLoader;


/**
 * Contains all of the information that defines the enemy FireGuy. This
 * includes its position, and also takes care of 
 * @author Timothy Miles
 *
 */
public class FireGuy extends Character{
	
	boolean moveRight = true;
	float distance = 0;
	
	/**
	 * Constructs a new fireguy instance with the default settings.
	 */
	public FireGuy(){
		this.init();
	}
	
	/**
	 * Constructs a new fireguy instance with new values for the scale and 
	 * position.
	 * @param newPosition The position that the fireguy should be placed at.
	 * @param scale The scale that should be used for converting the fireguy 
	 * image to a sprite.
	 */
	public FireGuy(Vector2f newPosition, float scale){
		position = newPosition;
		characterScale = scale;
		this.init();
	}
	
	/**
	 * Take care of the initialization of fireguy.
	 */
	protected void init() {
		duration = 150;
		jumpDuration = 100;
		spritesPerColumn = 5;
		character = DataLoader.FIRE_GUY; // Temp
		super.init();
		
		
		//move right animation
		Image[] moveRight= {sheet.getSprite(0,0) ,sheet.getSprite(1,0) , 
				sheet.getSprite(2,0) , sheet.getSprite(0,0) ,
				sheet.getSprite(3,0) , sheet.getSprite(4,0) ,
				sheet.getSprite(0,0)};
		super.characterMoveRight = new Animation(moveRight, duration, true);
		super.animation = super.characterMoveRight;
		
		//move left animation
		Image[] moveLeft = {sheet.getSprite(0,1) ,sheet.getSprite(1,1) , 
				sheet.getSprite(2,1) , sheet.getSprite(0,1) ,
				sheet.getSprite(3,1) , sheet.getSprite(4,1) ,
				sheet.getSprite(0,0)};
		super.characterMoveLeft = new Animation(moveLeft , duration, true);
	}
	
	/**
	 * Responsible for moving the position of the character.
	 * @param gc
	 * @param sb
	 * @param delta Time between frames.
	 * @param level Current level object the character is on.
	 */
	public void move(GameContainer gc, StateBasedGame sb, float delta, Level level){
		if(distance >= 90.0) {
			if(moveRight) {
				moveRight = false;
				distance = 0;
			} else {
				moveRight = true;
				distance = 0;
			}
		} 
		if(moveRight) {
			position.x += delta;
			distance += delta;
		} else {
			position.x -= delta;
			distance += delta;
		}	
	}
}
