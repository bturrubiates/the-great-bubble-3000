package character;

import level.Level;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import utils.DataLoader;


/**
 * 
 * @author Ben Turrubiates
 *
 */
public class Bubble extends Character { 

	public static int score = 0;
	float oldLocation = 80;
	
	/**
	 * Creates new Bubble instance with default fields.
	 */
    public Bubble() {
    	this.init();
    }
    
    /**
     * Creates a Bubble instance with the given position and scale.
     * @param position Starting position for the Bubble.
     * @param scale Scale used when creating the bubble sprite out of the
     * original image.
     */
	public Bubble(Vector2f position, float scale){
		this.position = position;
		characterScale = scale;
		this.init();
	}
	
    @Override
    /**
     * Initialize a new Bubble instance. Calls the parent init function, and
     * takes care of any extra initialization needed for this specific instance.
     */
	protected void init() {
		duration = 150;
		jumpDuration = 300;
		spritesPerColumn = 5;
		character = DataLoader.BUBBLE_IMAGE;
		super.init();
		
		
		//move right animation
		Image[] moveRight= {sheet.getSprite(2,0) ,sheet.getSprite(1,0) , 
				sheet.getSprite(0,0) , sheet.getSprite(1,0) ,
				sheet.getSprite(2,0) , sheet.getSprite(3,0) ,
				sheet.getSprite(4,0) , sheet.getSprite(3,0)};
		super.characterMoveRight = new Animation(moveRight, duration, true);
		super.animation = super.characterMoveRight;
		
		//move left animation
		Image[] moveLeft = {sheet.getSprite(2,1) ,sheet.getSprite(1,1) , 
				sheet.getSprite(0,1) , sheet.getSprite(1,1) ,
				sheet.getSprite(2,1) , sheet.getSprite(3,1) ,
				sheet.getSprite(4,1) , sheet.getSprite(3,1)};
		super.characterMoveLeft = new Animation(moveLeft , duration, true);
		
		//jump right animation
		Image[] jumpRight= {
				sheet.getSprite(1,2) , sheet.getSprite(2,2)};
		super.characterJumpRight = new Animation(jumpRight, jumpDuration, true);
		
		//jump left animation
		Image[] jumpLeft= {
				sheet.getSprite(1,3) , sheet.getSprite(2,3)};
		super.characterJumpLeft = new Animation(jumpLeft, jumpDuration, true);
		
		Image[] die = {
				sheet.getSprite(0,4), sheet.getSprite(1,4) ,
				sheet.getSprite(2,4)};
		super.characterDie = new Animation(die, jumpDuration, true);
		
	}
	
	/**
	 * Used to change the score tracking location.
	 * @param location New furthest location.
	 */
	public void setMaxLocation(int location) {
		oldLocation = location;
	}
	
	/**
	 * Update the position of the Bubble.
	 * @param gc 
	 * @param sb
	 * @param delta
	 * @param level
	 */
	public void update(GameContainer gc, StateBasedGame sb, int delta,
			Level level) {
		super.move(gc,sb,delta, level);
	    if(position.x > oldLocation){ //If larger than largest
	    	score++;
	    	oldLocation = position.x;
	    }   
	}
	
}
