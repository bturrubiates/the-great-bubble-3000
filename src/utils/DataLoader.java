package utils;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

/**
 * Takes care of loading all the resources associated with the game.
 * @author Benjamin Turrubiates
 *
 */
public class DataLoader {
	public static Image CLOUDS, GRAY_TREE, MOUNTAINS; 
	public static Image MAINMENU_IMAGE, PAUSEMENU_IMAGE, GAMEOVER_IMAGE;
	public static Image BUBBLE_IMAGE, VOLCANO_IMAGE;
	public static Image GRASS;
	public static Image BLUE, GREEN, ORANGE, RED;
	public static Image SPIKE, LEVEL_FINISH;
	public static Image DIRT, DIRT_LONG;
	public static Image BLACK, BLACK_LONG;
	
	// Enemies
	public static Image FIRE_GUY;
	
	public static TrueTypeFont FONT;

	/**
	 * Loads all of the resources into static variables
	 * @throws SlickException
	 * @throws FontFormatException
	 * @throws IOException
	 */
	public static void loadResources() 
			throws SlickException, FontFormatException, IOException {
		CLOUDS = 
			new Image("/data/images/background/cloudsBackgroundResized.png");
		GRAY_TREE =
			new Image("/data/images/background/GrayTreeBackground.png");
		MOUNTAINS = 
			new Image("/data/images/background/MountainBackgroundResized.png");
		MAINMENU_IMAGE = 
			new Image("/data/images/menu/TitleMenuBaseResized.png");
		GAMEOVER_IMAGE = 
			new Image("/data/images/menu/GameOver.png");
		PAUSEMENU_IMAGE =
			new Image("/data/images/menu/PauseMenuResized.png");
		BUBBLE_IMAGE = 
			new Image("/data/images/sprite/BubbleSpriteExtended.png");
		VOLCANO_IMAGE =
			new Image("/data/images/sprite/volcanoTitleSprite.png");
		
		GRASS = 
			new Image("/data/images/platforms/grass/GrassAltered.png");
		GRASS = GRASS.getScaledCopy(.6f);
		
		BLUE 	= 
				new Image("/data/images/platforms/colors/ColorBlue.png");
		BLUE = BLUE.getScaledCopy(.6f);
			
		GREEN 	= 
				new Image("/data/images/platforms/colors/ColorGreen.png");
		GREEN = GREEN.getScaledCopy(.6f);	
			
		ORANGE 	= 
				new Image("/data/images/platforms/colors/ColorOrange.png");
		ORANGE = ORANGE.getScaledCopy(.6f);
			
		RED 	= 
				new Image("/data/images/platforms/colors/ColorRed.png");
		RED = RED.getScaledCopy(.6f);
			
		SPIKE 	=
				new Image("/data/images/platforms/Spike.png");
		SPIKE = SPIKE.getScaledCopy(.18f);
		
		LEVEL_FINISH 	=
				new Image("/data/images/platforms/LevelFinish.png");
		LEVEL_FINISH = LEVEL_FINISH.getScaledCopy(.6f);
		
		DIRT 	=
				new Image("/data/images/platforms/dirt/DirtBase.png");
		DIRT = DIRT.getScaledCopy(.4f);
		
		DIRT_LONG = 
				new Image("/data/images/platforms/dirt/DirtLong.png");
		DIRT_LONG = DIRT_LONG.getScaledCopy(.4f);
		
		BLACK = 
				new Image("/data/images/platforms/black/BlackBasic.png");
		BLACK = BLACK.getScaledCopy(.4f);
		
		BLACK_LONG = 
				new Image("/data/images/platforms/black/BlackLong.png");
		BLACK_LONG = BLACK_LONG.getScaledCopy(.4f);
		
		
		/* Enemies */
		FIRE_GUY = new Image("/data/images/sprite/FireGuySpriteExtended.png");
		FIRE_GUY = FIRE_GUY.getScaledCopy(.4f);
		
		InputStream inputStream =
				ResourceLoader.getResourceAsStream ("/data/Arvo-Bold.ttf");
		Font newFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
		newFont = newFont.deriveFont(32f);
		FONT = new TrueTypeFont(newFont, true);
	}
}