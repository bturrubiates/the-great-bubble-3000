package states;

import menu.*;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.transition.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import utils.DataLoader;
  
/**
 * Class containing all of the code for the pause menu.
 * @author Benjamin Turrubiates
 *
 */
public class PauseMenuState extends BasicGameState{
  
    private int stateId = 0;
    
    private Menu pauseMenu;
    
    /**
     * 
     * @param state Set the ID that represents this state.
     */
    PauseMenuState(int state){
    	stateId = state; 
    	
    }
    
    /**
     * 
     * Initialize the pause menu. 
     * @param gc
     * @param sb
     * 
     */
	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		pauseMenu = new Menu();
		
		pauseMenu.addItem(new MenuItem("Continue") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				sb.enterState(GreatBubble3000.GAMEPLAYSTATE,
						new EmptyTransition(),
						new HorizontalSplitTransition(Color.white));
			}
		});
		pauseMenu.addItem(new MenuItem("Quit") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				System.exit(0);
			}
		});
		
	}

	/**
	 * Renders the pause menu and the background image.
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		
		DataLoader.PAUSEMENU_IMAGE.draw(0, 0, gc.getWidth(), gc.getHeight());
		pauseMenu.render(g);
	}

	/**
	 * Responsible for updating the selected option in the menu.
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int arg2)
			throws SlickException {
		// Pause game music for pause screen
		//SoundStore.get().pauseLoop();
		pauseMenu.update(Mouse.getX(), Mouse.getY(), sb, gc);
	}

	/**
	 * Retreive the ID representing this state.
	 */
	@Override
	public int getID() {
		return stateId;
	}
 
}