package states;

import menu.Menu;
import menu.MenuItem;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import character.Bubble;

import utils.DataLoader;

/**
 * Class containing everything for the game over screen. 
 * @author Benjamin Turrubiates
 *
 */
public class GameOverState extends BasicGameState{
  
    private int stateId = 0;
    private Menu gameOverMenu;
    
    /**
     * 
     * @param state Slick state ID.
     */
    GameOverState(int state){
    	stateId = state; // should be 1
    	
    }
	
    /**
     * Initialize the game over state.
     * @param gc
     * @param sb
     */
    @Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		gameOverMenu = new Menu();
		gameOverMenu.addItem(new MenuItem("Score: "){
			public void selectOption(StateBasedGame sb, GameContainer gc){
				
			}
		});
		gameOverMenu.addItem(new MenuItem("Retry") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				int newState = GreatBubble3000.lastState + 1;
				sb.addState(
					new GameplayState(newState));
				GreatBubble3000.GAMEPLAYSTATE = newState;	
				GreatBubble3000.lastState = newState;
				try {
					sb.getState(newState).init(gc, sb);
				} catch (SlickException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				sb.enterState(GreatBubble3000.GAMEPLAYSTATE);
			}
		});
		gameOverMenu.addItem(new MenuItem("Quit") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				System.exit(0);
			}
		});
	}

    /**
     * Render the game over screen. This includes the background, menu, and
     * score
     * 
     * @param gc
     * @param sb
     * @param g
     */
	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		DataLoader.GAMEOVER_IMAGE.draw(0, 0, gc.getWidth(), gc.getHeight());
		gameOverMenu.getList().get(0).setName("Score: " + Bubble.score);
		gameOverMenu.render(g);
		
	}

	/**
	 * Keeps track of the selected option in the menu.
	 * 
	 * @param gc
	 * @param sb
	 * @param delta
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
		gameOverMenu.update(Mouse.getX(), Mouse.getY(), sb, gc);
	}
	
	/**
	 * 
	 * @return The state ID corresponding to this state. 
	 */
	@Override
	public int getID() {
		return stateId;
	}
 
}