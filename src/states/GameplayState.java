package states;

import java.util.Stack;

import level.Level;
import menu.Menu;
import menu.MenuItem;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.transition.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import character.Bubble;
import character.FireGuy;

/**
 * Class that contains the logic for the gameplay state.
 * @author Benjamin Turrubiates
 *
 */
public class GameplayState extends BasicGameState{
  
    private int stateId = 0;
    Bubble bubble = null;
    Level level;
    private Menu statsMenu;
    private Stack<String> levelTitles = new Stack<String>();
    private String[] titles = {"BEST LEVEL EVAR", "OTHER LEVAL",
    		"COOL LEVAL", "MOAR NAME", "OTHER MOAR NAME", "TH1S 1 IS BEST",
    		"BUT WHO IS CAMERA", "CORRECT HORSE VOLCANO BUBBLE"};
    
    //private Audio gameMusic = null;
    /**
     * 
     * @param state Number to assign to this state.
     */
    GameplayState(int state){
    	stateId = state; // should be 1
    	
    }

    /**
     * Initialize the gameplay state.
     * @param gc
     * @param sb
     */
	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		Bubble.score = 0;
		
		if(levelTitles.isEmpty())
			for(String title : titles)
				levelTitles.add(title);
		if(levelTitles.size() == titles.length)
			level = new Level(levelTitles.pop(), 10000, 600);
		Vector2f firstBlock = level.getFirstPosition();
		bubble = new Bubble();
		Vector2f newPosition = new Vector2f(firstBlock.x,
				firstBlock.y - bubble.getHeight());
		bubble.setPosition(newPosition);
		statsMenu = new Menu(700, 20);
		
		statsMenu.addItem(new MenuItem("Score: " + Bubble.score) {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				
			}
		});
		
/*		try {
			gameMusic = AudioLoader.getStreamingAudio("OGG", 
					ResourceLoader.getResource(
							"/data/sounds/RealmOfZeltroz.ogg"));
		} catch (IOException e) {
			System.err.println("IOException: " + e.getMessage());
			e.printStackTrace();
		}*/
	}
	
	/**
	 * Render everything associated with the gameplay.
	 * @param gc
	 * @param sb
	 * @param g
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		level.getBackground().draw(0, 0, gc.getWidth(), gc.getHeight());
		level.render(bubble);
		statsMenu.render(g);

	}

	/**
	 * 
	 * Keep all of the state involved in gameplay updated.
	 * 
	 * @param gc
	 * @param sb
	 * @param delta
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
/*		if(! gameMusic.isPlaying())
			gameMusic.playAsMusic(1.0f, 1.0f, true);*/
		Input input = gc.getInput();
		
		bubble.update(gc, sb, delta, level);
		// Enter game over state if the player dies.
		if(bubble.isDead())
			sb.enterState(GreatBubble3000.GAMEOVERSTATE,
					new FadeOutTransition(Color.white, 1000),
					new FadeInTransition(Color.white, 1000));
		// If the player won a level attempt to go to the next level
		// If there are no levels then go to the game over state.
		if(level.getWon()) {
			if(!levelTitles.empty()) {
				bubble.setMaxLocation(0);
				level = new Level(levelTitles.pop(), 10000, 600);
				Vector2f firstBlock = level.getFirstPosition();
				Vector2f newPosition = new Vector2f(firstBlock.x,
						firstBlock.y - bubble.getHeight());
				bubble.setPosition(newPosition);
			} else {
				sb.enterState(GreatBubble3000.GAMEOVERSTATE,
						new FadeOutTransition(Color.white, 1000),
						new FadeInTransition(Color.white, 1000));
			}
		}
		
		// Enter the pause menu when P is pressed.
		if(input.isKeyPressed(Input.KEY_P)){
			sb.enterState(GreatBubble3000.PAUSEMENUSTATE,
					new EmptyTransition(),
					new HorizontalSplitTransition(Color.white));
		}
		// Display the score in the top right
		statsMenu.getList().get(0).setName("Score: " + Bubble.score);
		statsMenu.update(0, 0, sb, gc);
		
		// Handle the primitive movement of the enemies.
		for(FireGuy enemy: level.getEnemyList()){
			enemy.move(gc, sb, 0.5f, level);
		}
	}

	/**
	 * @return ID associated with this state.
	 */
	@Override
	public int getID() {
		return stateId;
	}
 
}