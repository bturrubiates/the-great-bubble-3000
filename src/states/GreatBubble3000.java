package states;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
  
/**
 * The state representing the game.
 * @author Ky Brabson and Timmy Miles
 *
 */
public class GreatBubble3000 extends StateBasedGame {
  
    public static int MAINMENUSTATE          = 0;
    public static int GAMEPLAYSTATE          = 1;
    public static int MULTIPLAYERGAMESTATE   = 2;
    public static int PAUSEMENUSTATE         = 3;
    public static int GAMEOVERSTATE          = 4;
    public static int lastState = GAMEOVERSTATE;
    
    /**
     * Create a new instance of the GreatBubble3000 game.
     */
    public GreatBubble3000() {
        super("GreatBubble3000"); // Name on window
  
        // Adds states
        this.addState(new MainMenuState(MAINMENUSTATE));
        this.addState(new GameplayState(GAMEPLAYSTATE));
        this.addState(new PauseMenuState(PAUSEMENUSTATE));
        this.addState(new GameOverState(GAMEOVERSTATE));
        this.addState(new MultiPlayerGameState(MULTIPLAYERGAMESTATE));
        
        this.enterState(MAINMENUSTATE);// Initial state
    }
  
    public static void main(String[] args) throws SlickException {
    	// Creates new greabubble3000 game
        AppGameContainer app = new AppGameContainer(new GreatBubble3000()); 
        // Sets window specs, NO fullscreen, beware sets USER resolution
        app.setDisplayMode(1000, 600, false);
        app.setVSync(true);
        app.setTargetFrameRate(60);
        app.setMaximumLogicUpdateInterval(10);
        app.start();
    }
    
    /**
     * Initiate the states list.
     */
    @Override
    public void initStatesList(GameContainer gameContainer) 
    		throws SlickException {
        this.getState(MAINMENUSTATE).init(gameContainer, this);
        this.getState(GAMEPLAYSTATE).init(gameContainer, this);
        this.getState(PAUSEMENUSTATE).init(gameContainer, this);
        this.getState(GAMEOVERSTATE).init(gameContainer, this);
        this.getState(MULTIPLAYERGAMESTATE).init(gameContainer, this);
    }
    
}