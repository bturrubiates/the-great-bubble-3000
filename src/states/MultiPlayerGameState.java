package states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Not implemented
 * @author Ky Brabson, Timmy Miles
 *
 */
public class MultiPlayerGameState extends BasicGameState{
  
    private int stateId = 0;
  
    
    MultiPlayerGameState(int state){
    	stateId = state; // should be 1
    	
    }

	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int arg2)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return stateId;
	}
 
}