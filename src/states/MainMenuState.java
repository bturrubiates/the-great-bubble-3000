package states;

import java.awt.FontFormatException;
import java.io.IOException;

import org.lwjgl.input.Mouse;
import utils.DataLoader;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.*;
import character.*;
import menu.*;
  
/**
 * Contains all the logic associated with the main menu.
 * @author Ben Turrubitaes, Ky Brabson
 *
 */
public class MainMenuState extends BasicGameState{
  
	private Menu mainMenu;
	
    private int stateId = 0;
    //private Audio menuSong;

    Bubble bubble;
    Volcano volcano;
    
    /**
     * 
     * @param state Number representing this state.
     */
    MainMenuState(int state){
    	stateId = state;
    	
    }

    /**
     * Initialize the main menu.
     */
	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		try {
			DataLoader.loadResources();
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}

		mainMenu = new Menu();
		
		mainMenu.addItem(new MenuItem("Single Player") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				sb.enterState(GreatBubble3000.GAMEPLAYSTATE,
						new FadeOutTransition(Color.white, 1000),
						new FadeInTransition(Color.white, 1000));
			}
		});
		/*mainMenu.addItem(new MenuItem("Multiplayer") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				sb.enterState(GreatBubble3000.MULTIPLAYERGAMESTATE,
						new FadeOutTransition(Color.white, 1000),
						new FadeInTransition(Color.white, 1000));
			}
		});
		*/
		mainMenu.addItem(new MenuItem("Quit") {
			public void selectOption(StateBasedGame sb, GameContainer gc) {
				System.exit(0);
			}
		});
				
/* 		try {
			menuSong = AudioLoader.getStreamingAudio("OGG", 
					ResourceLoader.getResource(
							"/data/sounds/SomethingStrange.ogg"));
		} catch (IOException e) {
			System.err.println("IOException: " + e.getMessage());
			e.printStackTrace();
		}*/
		
		//creates animation for the title menu
		bubble = new Bubble(new Vector2f(195.0f, 355.0f),0.6f);
		volcano = new Volcano(105,150,0.4f);
		
	}

	/**
	 * Render the main menu and all resources associated with it. 
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
			
		DataLoader.MAINMENU_IMAGE.draw(0, 0, gc.getWidth(), gc.getHeight());
		bubble.render(0);
		volcano.render();
		mainMenu.render(g);
			
	}

	/**
	 * Responsible for updating the selected item in the menu.
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int arg2)
			throws SlickException {
		
		// Turned off temporarily for performance reasons. 
/*		if(! menuSong.isPlaying())
			menuSong.playAsMusic(1.0f, 1.0f, true);*/
		
		mainMenu.update(Mouse.getX(), Mouse.getY(), sb, gc);
	}

	/**
	 * Get the ID that represents this state
	 */
	@Override
	public int getID() {
		return stateId;
	}
 
}