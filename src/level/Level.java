package level;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import character.*;

import utils.DataLoader;

/**
 * Class that holds all the information associated with the levels of the game.
 * @author Benjamin Turrubiates
 *
 */
public class Level {
	private Rectangle viewingArea = new Rectangle(0, 0, 1000, 600);
	private int levelWidth, levelHeight;
	private ArrayList<Block> levelBlocks;
	private ArrayList<FireGuy> enemies;
	public Image background;
	private boolean won = false;
	private String[] backgroundOptions = {"CLOUDS", "GRAY_TREE", "MOUNTAINS"};
	
	/**
	 * Constructs a default level
	 * @param name Level name can then be used to draw name when entering level
	 * @param width width of the level in absolute pixels
	 * @param height height of the level in absolute pixels
	 */
	public Level(String name, int width, int height) {
		levelWidth = width;
		levelHeight = height;
		
		Random random = new Random();
		String background = backgroundOptions[
                                  random.nextInt(backgroundOptions.length)];
		setBackground(background);
		
		levelBlocks = LevelGenerator.generateTerrain(background, levelWidth,
				levelHeight);
		enemies = LevelGenerator.spawnEnemy(levelBlocks);
	}
	
	/**
	 * 
	 * @return The position of the first block
	 */
	public Vector2f getFirstPosition() {
		return levelBlocks.get(0).getPosition();
	}
	
	/**
	 * Updates the level status
	 * @param status Whether the player has won the level or not
	 */
	public void setWon(boolean status) {
		this.won = status;
	}
	
	/**
	 * 
	 * @return Whether the level has been won or not
	 */
	public boolean getWon() {
		return won;
	}
	
	/**
	 * 
	 * @param backgroundName The new background for the level
	 */
	private void setBackground(String backgroundName) {
		switch(backgroundName) {
			case "CLOUDS":
				this.background = DataLoader.CLOUDS;
				break;
			case "GRAY_TREE":
				this.background = DataLoader.GRAY_TREE;
				break;
			case "MOUNTAINS":
				this.background = DataLoader.MOUNTAINS;
				break;
		}
	}
	
	/**
	 * 
	 * @return Get the list of blocks in the level
	 */
	public ArrayList<Block> getBlockList() {
		return levelBlocks;
	}
	
	/**
	 * 
	 * @return Get the list of enemies in the level
	 */
	public ArrayList<FireGuy> getEnemyList() {
		return enemies;
	}
	
	/**
	 * 
	 * @return Get the current background
	 */
	public Image getBackground() {
		return this.background;
	}
	
	/**
	 * Draws all of the resources in the level
	 */
	public void render(Bubble mainPlayer) {
		// Don't let the character go beyond the width of the level
		if((mainPlayer.getX() + mainPlayer.getWidth()) > levelWidth)
			mainPlayer.setX(levelWidth - mainPlayer.getWidth());
		int characterCenter = (int) Math.round(mainPlayer.getCenter().x);
		// The character should always be somewhat centered in the screen
		int viewingX = characterCenter - (viewingArea.width / 2);
		// If the viewing area X coordinate is less than 0, then make it 0
		if(viewingX < 0)
			viewingX = 0;
		// If the viewing area X coordinate is greater than the level width
		// then set it to the level width minus the width of the viewingArea.
		else if(viewingX > levelWidth) {
			viewingX = levelWidth - viewingArea.width;
		}
		// Set the position that we're actually viewing
		viewingArea.setLocation(viewingX, 0);
		// Mainplayer will do the math to offset the viewing area
		mainPlayer.render(viewingX);
		// Check if the block is in the current window, and if it is render it
		for(Block block : levelBlocks) {
			if(viewingArea.intersects(block.getRectangularHitBox())) {
				block.render(viewingX);
			}
		}
		for(FireGuy enemy : enemies){
			enemy.render(viewingX);
		}
	}
}