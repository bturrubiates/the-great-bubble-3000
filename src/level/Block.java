package level;

import utils.DataLoader;
import org.apache.commons.lang3.ArrayUtils;

import java.awt.Rectangle;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

/**
 * Contains all of the information for the blocks that build up levels. This
 * includes the image, the x coordinate, y coordinate, and also takes care of 
 * what blocks can be introduced into which environments.
 * @author Benjamin Turrubiates
 *
 */
public class Block {
	
	private Vector2f position;
	private Image image;
	private String blockType;
	
	private static String[] mountainOptions = {"DIRT", "DIRT_LONG"};
	private static String[] grayTreeOptions = {"BLACK", "BLACK_LONG"};
	private static String[] cloudOptions 	 = {"GRASS"};
	private static String[] randomOptions 	 = 
		{"BLUE", "GREEN", "ORANGE", "RED", "SPIKE"};
	
	/**
	 * 
	 * @return Hit box for the current block.
	 */
	public Rectangle getRectangularHitBox() {
		int x = (int) Math.round(position.x);
		int y = (int) Math.round(position.y);
		int width = image.getWidth(); 
		int height = image.getHeight();
		
		if(blockType == "SPIKE") {
			x += 12;
			width -= 24;
		}
		return new Rectangle(x, y, width, height);
	}
	
	/**
	 * Creates a new block object that can be used in a level object
	 * to describe the blocks that make up the level
	 * 
	 * @param blockType Type of block being generated
	 * @param x			X coordinate where the block will be drawn
	 * @param y			Y coordinate where the block will be drawn
	 */
	public Block(String blockType, int x, int y) {
		position = new Vector2f(x, y);
		this.blockType = blockType;
		switch(blockType) {
			case "BLUE":
				image = DataLoader.BLUE;
				break;
			case "GREEN":
				image = DataLoader.GREEN;
				break;
			case "RED":
				image = DataLoader.RED;
				break;
			case "ORANGE":
				image = DataLoader.ORANGE;
				break;
			case "SPIKE":
				image = DataLoader.SPIKE;
				break;
			case "GRASS":
				image = DataLoader.GRASS;
				break;
			case "DIRT":
				image = DataLoader.DIRT;
				break;
			case "DIRT_LONG":
				image = DataLoader.DIRT_LONG;
				break;
			case "BLACK":
				image = DataLoader.BLACK;
				break;
			case "BLACK_LONG":
				image = DataLoader.BLACK_LONG;
				break;
			case "LEVEL_FINISH":
				image = DataLoader.LEVEL_FINISH;
				break;
			default:
				System.err.println("This isn't a block I recognize");
				throw new IllegalArgumentException("Unrecognized block");
		}
	}
	
	/**
	 * 
	 * @return A string representation of the current block.
	 */
	public String getBlockType() {
		return this.blockType;
	}
	
	/**
	 * 
	 * @return The width of the image being used to represent the block object.
	 */
	public int getWidth()  {
		return image.getWidth();
	}
	
	/**
	 * 
	 * @return The center of the block.
	 */
	public Vector2f getCenter() {
		return new Vector2f((position.x + getWidth())/2,
				(position.y + getHeight())/2);
	}
	
	/**
	 * 
	 * @return The height of the image being used to represent the block object
	 */
	public int getHeight() {
		return image.getHeight();
	}
	
	/**
	 * 
	 * @return Vector representing the position of the block.
	 */
	public Vector2f getPosition() {
		return position;
	}
	
	/**
	 * 
	 * @param y Set the Y coordinate of the block.
	 */
	public void setY(float y) {
		position.y = y;
	}
	
	/**
	 * 
	 * @param x Set the X coordinate of the block.
	 */
	public void setX(float x){
		position.x = x; 
	}
	
	/**
	 * Allows for the random generator to figure out which blocks go in which
	 * environments.
	 * @param background A string representing the environment type
	 * @return A list of strings representing the block options
	 */
	public static String[] getBlockOptions(String background) {
		switch(background) {
			case "CLOUDS": 
				return ArrayUtils.addAll(cloudOptions, randomOptions);
			case "GRAY_TREE":
				return ArrayUtils.addAll(grayTreeOptions, randomOptions);
			case "MOUNTAINS":
				return ArrayUtils.addAll(mountainOptions, randomOptions);
			default:
				System.err.println("I don't recognize this background");
				throw new IllegalArgumentException("Unrecognized background");
		}
	}
	
	/**
	 * Uses the draw function to render the image representation of the object
	 * to the screen
	 */
	public void render(int viewingArea) {
		image.draw((position.x - viewingArea), position.y);
	}
}