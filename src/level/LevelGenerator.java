package level;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.geom.Vector2f;

import character.FireGuy;

/**
 * Class used to populate the level with random terrain.
 * @author Benjamin Turrubiates
 *
 */
public class LevelGenerator {
	
	private static String[] BLOCK_OPTIONS;
	
	private static int[] JUMP_OPTIONS = 
		{1, 140, 180, 190};
	
	
	/**
	 * 
	 * @param blocks List of blocks in the level.
	 * @return List of enemies spawned for the level.
	 */
	public static ArrayList<FireGuy> spawnEnemy(ArrayList<Block> blocks){
		ArrayList<FireGuy> enemies = new ArrayList<FireGuy>();
		FireGuy enemy;
		Random random = new Random();
		Block randomBlock;
		int blockIndex;
		for(int i = 0; i < 5; i++){
			blockIndex = random.nextInt(blocks.size());
			randomBlock = blocks.get(blockIndex);			
			enemy = new FireGuy(new Vector2f(0, 0), .8f);
			
			/* Makes sure no instant death*/
			while(blockIndex == 0){ // First block of level
				blockIndex = random.nextInt(blocks.size()); // Selects new block index
			}
			
			
			/* Makes sure no stacking of enemies */
			if(enemies.size() > 0){ // Checks non empty array list
				for(FireGuy fireguy : enemies){ // Go through all the elements
					if((randomBlock.getPosition().x) == fireguy.getX()){
						blockIndex = random.nextInt(blocks.size()); // Selects new block
					}
				}
			}
			
			randomBlock = blocks.get(blockIndex);
			enemy.setPosition(
					new Vector2f(randomBlock.getPosition().x,
							randomBlock.getPosition().y - enemy.getHeight()));
			enemies.add(enemy);
		}
		return enemies;
	}
	
	/**
	 * Method to generate terrain. Will use the level size to generate
	 * terrain more appropriately.
	 * @return ArrayList of Block structures that make up the level.
	 * This includes hazards.
	 */
	public static ArrayList<Block> generateTerrain(String background, int width,
			int height) {
		Random random = new Random();
		BLOCK_OPTIONS = Block.getBlockOptions(background);
		ArrayList<Block> blocks = new ArrayList<Block>();
		
		int previousX = 20;
		int previousY = 500;
		
		boolean check = false;
		Block firstBlock = null;
		while(!check){
			firstBlock = new Block(
					BLOCK_OPTIONS[random.nextInt(BLOCK_OPTIONS.length)],
					previousX, previousY);
			if(firstBlock.getBlockType() != "SPIKE"){
				check = true;
			}
			previousX += firstBlock.getWidth() + 
					JUMP_OPTIONS[random.nextInt(JUMP_OPTIONS.length)];
		}
		blocks.add(firstBlock);
		
		while(previousX < width) {
			Block tempBlock = new Block(
					BLOCK_OPTIONS[random.nextInt(BLOCK_OPTIONS.length)],
					previousX, previousY);
			if((previousX + tempBlock.getWidth() + 500) > width){
				tempBlock = new Block("LEVEL_FINISH",
						previousX, previousY);
				blocks.add(tempBlock);
				tempBlock = new Block("LEVEL_FINISH",
						previousX+190, previousY);
				blocks.add(tempBlock);
				break;
			}
			if(tempBlock.getBlockType() == "SPIKE")
			{
				blocks.add(new Block("DIRT", previousX, previousY));
				previousX += 78;
				tempBlock.setX(previousX);// Update
				tempBlock.setY(previousY - 35);
			
			    Block underSpike = new Block(
			    		"DIRT", previousX, previousY);
			    blocks.add(underSpike);
			    Block nextSpike = new Block(
			    		"DIRT", previousX+78, previousY);
			    blocks.add(nextSpike);
			    blocks.add(tempBlock);
			    
				previousX += tempBlock.getWidth() + 
						120;
			}
			//if(blocks.get(counter).getBlockType() )
			blocks.add(tempBlock);
			previousX += tempBlock.getWidth() + 
					JUMP_OPTIONS[random.nextInt(JUMP_OPTIONS.length)];
		}
		return blocks;
	}
}