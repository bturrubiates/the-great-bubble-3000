package menu;

import utils.DataLoader;
import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.geom.Vector2f;

/**
 * Handles the drawing of the menu, and all of the resources associated with
 * a menu including the menu options. 
 * @author Benjamin Turrubiates
 *
 */
public class Menu {
	private ArrayList<MenuItem> menuOptions;
	private TrueTypeFont font;
	private Color normalColor = Color.blue;
	private Color activeColor = Color.red;
	private Vector2f framePos;
	private float verticalSpacing = 50.0f;
	private float rectangleWidth;
	
	/**
	 * Declare a new menu object. Contains default positions that cannot
	 * be overridden at the moment.
	 */

	public Menu() {
		framePos = new Vector2f(720.0f, 290.0f);
		menuOptions = new ArrayList<MenuItem>();
		font = DataLoader.FONT;
	}
	public Menu(float x, float y){
		framePos = new Vector2f(x, y);
		menuOptions = new ArrayList<MenuItem>();
		font = DataLoader.FONT;
	}
	
	/**
	 * Handles rendering the menu and all of the menu items. Uses a fixed
	 * distance for each menu item. 
	 * @param g Graphics object to render the menu on. 
	 */
	public void render(Graphics g) {
		g.setColor(new Color(211, 211, 211, .5f));
		String longestOption = "";
		
		float yValue = framePos.y + 10;
		float xValue = framePos.x + 10;
		
		for(MenuItem i : menuOptions) {
			if(i.toString().length() > longestOption.length()) {
				longestOption = i.toString();
			}
			i.setY(yValue);
			yValue += verticalSpacing;
		}
		float textWidth = font.getWidth(longestOption);
		rectangleWidth = textWidth + 20;
		float rectangleHeight = menuOptions.size() * verticalSpacing + 10;
		g.fillRoundRect(framePos.x, framePos.y, 
				rectangleWidth, rectangleHeight, 8);
		
		// The color of the menu option depends on whether it is
		// being hovered over or not. A red font signifies it is being
		// highlighted.
		for(MenuItem i : menuOptions) {
			if(i.isHoveredOver() == true)
				font.drawString(xValue, i.getY(), i.toString(), activeColor);
			else
				font.drawString(xValue, i.getY(), i.toString(), normalColor);
		}
	}
	
	/**
	 * Updates the selected items. If the mouse is over one of the items, then
	 * the item is drawn in a red font. If the mouse button is down, then the 
	 * new state is entered.
	 * @param currentX The current X position of the mouse.
	 * @param currentY The current Y position of the mouse.
	 * @param sb Used to transition into a selected state.
	 */
	public void update(int currentX, int currentY, StateBasedGame sb,
			GameContainer gc) {
		if(currentX >= framePos.x
				&& currentX <= framePos.x + rectangleWidth) {
			
			// If we are inside one of the menu options then highlight it
			// if we select it, then enter the state
			float yValue = menuOptions.get(0).getY();
			for(MenuItem i : menuOptions) {
				if( currentY >= yValue - verticalSpacing
						&& currentY <= yValue) {
					i.setHoveredOver(true);
					
					if(Mouse.isButtonDown(0))
						i.selectOption(sb, gc);
				} else
					i.setHoveredOver(false);
				// This is because of the weird mouse being measured from
				// the bottom right. We need the inverse of the positions that
				// are stored in the menu items
				yValue -= 50;
			}
		} else {
			// If we aren't in the menu box then set all of them to false
			for(MenuItem i : menuOptions) {
				i.setHoveredOver(false);
			}
		}
	}
	
	/**
	 * 
	 * @return An ArrayList of the items in this menu object.
	 */
	public ArrayList<MenuItem> getList() {
		return this.menuOptions;
	}
	
	/**
	 * 
	 * @return The X position of the menu.
	 */
	public float getX() {
		return framePos.x;
	}
	
	/**
	 * 
	 * @return The Y position of the menu.
	 */
	public float getY() {
		return framePos.y;
	}
	
	/**
	 * 
	 * @return The width of the rectangle that is drawn behind the text.
	 */
	public float getWidth() {
		return this.rectangleWidth;
	}
	
	/**
	 * Used to add new menu options to the menu object. Need to give it a new
	 * MenuItem object that contains a name and an implemented select function.
	 * @param item Option to be added to the menu. 
	 */
	public void addItem(MenuItem item) {
		menuOptions.add(item);
	}
}
