package menu;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Abstract class that should be used to add new options to a menu object. 
 * @author Benjamin Turrubiates
 *
 */
public abstract class MenuItem {
	private String optionName;
	private Boolean hoveredOver;
	private float y;
	
	/**
	 * Create a new menu item with the given name, which will be displayed.
	 * @param name The name which will be displayed on the menu.
	 */
	public MenuItem(String name) {
		optionName = name;
		hoveredOver = false;
	}
	
	/**
	 * Query whether the menu item is currently being hovered over.
	 * @return Boolean representing whether the item is being hovered over.
	 */
	public Boolean isHoveredOver() {
		return this.hoveredOver;
	}
	
	/**
	 * Gets the Y value associated with this menu option. Represents the top
	 * left corner.
	 * @return Y value of top left corner of menu option.
	 */
	public float getY() {
		return this.y;
	}
	
	/**
	 * Allows the Y value for this menu option to be set.
	 * @param yValue Y value that the menu options Y value will be set to.
	 */
	public void setY(float yValue) {
		this.y = yValue;
	}
	
	/**
	 * Set the display name for the menu option.
	 * @param name The name that should be displayed for the option. 
	 */
	public void setName(String name) {
		optionName = name;
	}
	
	/**
	 * Marks the menu option as being hovered over.
	 * @param value State of the menu item, true if being hovered over. 
	 * False otherwise.
	 */
	public void setHoveredOver(Boolean value) {
		this.hoveredOver = value;
	}
	
	/**
	 * Must be implemented when declaring a new MenuItem. Dictates what to do 
	 * when the menu option is selected.
	 * @param sb Used to transition into a new state based on the menu 
	 * selection.
	 */
	public abstract void selectOption(StateBasedGame sb, GameContainer gc);
	
	/**
	 * Returns the name of the menu option. Used for drawing the name in the
	 * menu.
	 */
	@Override
	public String toString() {
		return optionName;
	}
}